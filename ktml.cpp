/**
 * Kavascript
 *
 * kavascript.cpp
 * snkemp
 * Feb 2018
 *
 * Translates kavascript files into HTML files.
 *
 **/

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include <fcntl.h>

#include <ftw.h>

#include <cstring>
#include <string>

#include <stdio.h>
#include <iostream>
#include <fstream>

#include <vector>
#include <map>
#include <stack>

#include "element.h"
#include "variable.h"

using namespace std;

void writeToFile( int fd, string l );
void writeToFile( int fd, char c );
char nextChar( int fd );
string nextString( int fd );
string nextWord( int fd );
string nextLine( int fd );
void parse(int fd, stack<variable> vars, element *curr);

int ktml( const char *source, char *destination, int flags ) {

    /* Check files */
    // Make sure they arent the same file
    if( !strcmp(source, destination) ) {
        printf("ktml: Illegal arguments. source and destination must be different, %s, %s\n", source, destination );
        return -1;
    }

    // Make sure source exists and is a file
    struct stat SRC;
    stat( source, &SRC );
    if( !S_ISREG(SRC.st_mode) ) {
        printf("ktml: Illegal argument %s. KS expects a file for the source.\n", source);
        return -1;
    }

    // Make sure destination is not a folder
    struct stat DEST;
    stat( destination, &DEST );
    if( S_ISDIR(DEST.st_mode) ) {
        printf("ktml: Illegal argument %s. KS cannot use a directory for the destinaiton.\n", destination );
        return -1;
    }  

    /* Open files */
    int src = open( source, O_RDONLY, 0777 );
    int dest = open( destination, O_CREAT | O_TRUNC | O_RDWR, 0777 );

    /* Run Kavascript */
    stack<variable> vars;
    element html("html", 0 );
    parse( src, vars, &html );
    
    // Write to file
    writeToFile( dest, "<!doctype html>" );
    html.writeToFile(dest);

    // Close files
    close( src );
    close( dest );

    return 0;
}

void parse( int fd, stack<variable> vars, element *curr ) {

    string word;

    while(true) {
        word = nextWord( fd );
        if( word.back() == EOF )
            return;

        // Ignore comments
        if( word == "/*" ) {
            while( word != "*/" )
                word = nextWord(fd);

            continue;
        }

        // Handle directives
        else
        if( word.front() == '@' ) {
            string params = nextWord(fd);
            if( word == "@filename" ) {
                // Get the tagname
                string tag = nextWord(fd);
                if( tag.back() != '{' )
                    nextWord(fd);
                else
                    tag.pop_back();

            
                // Make a file
                int src = open( params.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0777 );
            
                // Record the text in the file
                int count = 1;
                while( true ) {
                    char c = nextChar(fd);
                    if( c == EOF )
                        break;
                    if( c == '{' )
                        count++;
                    else
                    if( c == '}' )
                        count--;

                    if( count == 0 )
                        break;
                
                    write( src, &c, 1 );
                }

                close( src );

                element e(tag, curr->scope+1, true, params);
                curr->children.push_back(e);
                continue;
            }
            continue;
        }

        // Handle attributes
        else
        if( word.front() == '.' ) {
            string key = word.substr(1);
            string val = nextString(fd);
        
            if( key.back() == ':' )
                key.pop_back();

            attribute attr = {key, val};
            curr->attributes.push_back( attr );
            continue;
        }

        // Handle oneliners
        else
        if( word.back() == ':' ) {
            word.pop_back();
        
            string val = nextString(fd);
            element e(word, curr->scope+1, true, val);
            curr->children.push_back(e);
            continue;
        }

        // End scope
        else
        if( word == "}" ) {
            return;
        }
    
        // New tag
        else {
            // Handle style and scripts
            if( word == "style" || word == "script" ) {
                int count = 0;
                string script;
                do {
                    char c = nextChar(fd);
                    if( count != 0 )
                        script += c;

                   if( c == '{' )
                        count++;
                    else
                        if( c == '}' )
                            count--;

                } while( count != 0 );
            
                script.pop_back();
                element e(word, curr->scope+1, false, script);
                curr->children.push_back(e);
                continue;
            }

            // Handle for loops TODO
            string scope = nextWord(fd);
            if( scope == ":" ) {
                string val = nextString(fd);
                element e(word, curr->scope+1, true, val);
                curr->children.push_back(e);
                continue;
            }

            else
            if( scope == "{" ) {    
                element e(word, curr->scope+1);
                parse(fd, vars, &e);
                curr->children.push_back(e);
                continue;
            }
        }
    }
}

void writeToFile( int fd, string lines ) {
    int n = lines.length();

    // Copy each char into file
    for( int i = 0; i < n; i++ ) {
        writeToFile( fd, lines[i] );
    }
}

void writeToFile( int fd, char letter ) {

    char buffer[1];
    buffer[0] = letter;

    write( fd, buffer, 1 );
}

char nextChar( int fd ) {
    char buf[1];
    int nr = read( fd, buf, 1 );
    if( nr == 0 )
        buf[0] = EOF;
    
    return buf[0];
}

string nextWord( int fd ) {
    string word = "";
    char c;

    while( true ) {
        c = nextChar(fd);
        if( c == EOF )
            return word + c;
        else
        if( c == ' ' || c == '\t' || c == '\n' ) {
            if( word.empty() )
                continue;
            else
                break;
        }
        else
            word += c;
    }
    return word;
}

string nextString( int fd ) {

    string word = "";
    char c;

    while( true ) {
        c = nextChar(fd);
        if( c == EOF )
            break;
        else
        if( c == '\"' ) {
            if( word.empty() )
                continue;
            else
                break;
        }
        else
            word += c;
    }
    return word;
}

string nextLine( int fd ) {
    string line;
    char c;

    while( true ) {
        c = nextChar(fd);
        if( c == EOF )
            break;
        else
        if( c == '\n' && !line.empty() )
            break;

        line += c;
    }
    return line;
}
