default:
	@make -s -i all

all:
	make init run out

init:
	g++ -g -std=c++11 main.cpp -o ktml

run:
	ktml test.ktml

out:
	cat test.html
	echo ""

clean:
	rm my* test.html
