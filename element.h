// element.h
// snkemp

#include <list>
#include <string>

#include "attribute.h"

class element {
public: 
    int scope;
    string tag;

    list<attribute> attributes;
    list<element> children;

    bool singleton;
    bool meta;

    bool file;
    string filename;

    bool oneLiner;
    string innerHTML;

    element(string t, int s, int l=false, string h="") {

        // Tag and indentation
        tag = t;
        scope = s;

        // Handle css syntax for id and class
        int clss = tag.find('.');
        int id   = tag.find('#');
        
        if( clss > 0 ) {
            string className;
            if( id < clss )
                className = tag.substr(clss+1);
            else
                className = tag.substr(clss+1, id-clss-1);

            attributes.push_back({"class", className});
        }

        if( id > 0 ) {
            string idName;
            if( clss < id )
                idName = tag.substr(id+1);
            else
                idName = tag.substr(id+1, clss-id-1);

            attributes.push_back({"id", idName});
        }

        int min = id;
        if( clss < min && clss > 0 )
            min = clss;

        if( min >= 0 )
            tag = tag.substr(0, min-1);
        
        // Handle meta and singletons
        meta = tag == "meta";
        singleton = false;
        if( tag == "area" ||
            tag == "base" ||
            tag == "br" ||
            tag == "col" ||
            tag == "embed" ||
            tag == "hr" ||
            tag == "img" ||
            tag == "input" ||
            tag == "param" ||
            tag == "source" ||
            tag == "track" ||
            tag == "wbr" )
                singleton = true;

        // Handle file
        file = false;
        filename = "";

        // Handle oneLiners
        oneLiner = l;
        innerHTML = h;

        // Handle script/style attributes
        if( tag == "style" && oneLiner )
            tag = "link";

        if( tag == "link" && oneLiner ) {
            attributes.push_back({"rel", "stylesheet"});
            attributes.push_back({"type", "text/css"}); 
            attributes.push_back({"href", innerHTML});
            innerHTML = "";
        }
        else
        if( tag == "script" && oneLiner ) { 
            attributes.push_back({"type", "application/javascript"});
            attributes.push_back({"src", innerHTML});
            innerHTML = "";
        }
    }

    // Write to file
    void writeToFile( int fd ) {
        writeStringToFile( toString(), fd );
    }

    // Write string to file
    void writeStringToFile( string s, int fd ) {
        char buffer[s.length()];
        strcpy(buffer, s.c_str());
        write(fd, buffer, sizeof buffer);
    }

    string toString() {

        // scope number of tabs following a new line
        string tabs = "\n";
        for( int i = 0; i < scope; i++ )
            tabs += '\t';

        // ks sees meta data as a single tag with different attributes. HTML sees it as name/content pairs
        if( meta ) {
            string data;
            for( list<attribute>::iterator md = attributes.begin(); md != attributes.end(); md++ )
                data += tabs + "<meta name=\"" + md->key + "\" content=\"" + md->value + "\">";
            return data;
        }

        // Handle infile script and style
        if( (tag == "script" || tag == "style") && !oneLiner ) {
            string script = tabs + "<" + tag + ">";
            for( int i = 0; i < innerHTML.length(); i++ ) {
                if( innerHTML[i] == '\n' )
                    script += tabs;
                else
                    script += innerHTML[i];
            }
            return script + tabs + "</" + tag + ">";
        }

        // Add all the attributes
        string s = tabs + "<" + tag;
        for( list<attribute>::iterator attr = attributes.begin(); attr != attributes.end(); attr++ )
            s += " " + attr->key + "=" + "\"" + attr->value + "\"";


        s += ">";
        if( singleton )
            return s;

        for( list<element>::iterator e = children.begin(); e != children.end(); e++ )
            s += e->toString();

        s += innerHTML;
        
        // Close tag and return
        if( !oneLiner )
            s += tabs;
        
        s += "</" + tag + ">";
        return s;
    }
};
