/**
 * Kavascript
 * 
 * main.cpp
 * snkemp
 * Feb 2018
 *
 * Handles illegal arguments, passes
 * valid arguments to kavascript.cpp
 *
 * Expects arguments to be of form 
 *
 * ks [ -flags ] file.ks [output.html]
 *
 * Kavascript WILL overwrite output.html if it already exist and
 * create it if they do not.
 *
**/

#include <cstdlib>
#include <cstdio>

#include <cstring>
#include <string>

#include "ktml.cpp"

#define KTML_FLAGS ""

using namespace std;
int main( int n, char **args ) {

    // Variables to pass to ktml
    char *source = NULL,
         *destination = NULL;
    int flags = 0;

    // Evaluate each argument
    for( int i = 1; i < n; i++ ) {
        string arg(args[i]);

        // Handle flags
        if( arg[0] == '-' ) {
            for( int k = 1, l = arg.length(); k < l; k++ ) {

                // Find the index of the flag
                string ktml_flags(KTML_FLAGS);
                int f = ktml_flags.find(arg[k]);

                // If flag does not exist return
                if( f == -1 ) {
                    printf("ktml: Illegal flag: %c\n", arg[k] );
                    return -1;
                }

                // Set the fth bit
                flags |= 1 << f;
            }
        }

        else // We need a source
        if( source == NULL ) {
            source = args[i];
        }

        else // If we have a source, we are the destination
        if( destination == NULL ) {
            destination = args[i];
        }

        else { // Too many args
            printf("ktml: Illegal argument: %s\n", args[i] );
            return -1;
        }
    }

    // We require a source
    if( source == NULL ) {
        printf("ktml: No source file given\n" );
        return -1;
    }

    // We need a destination, but can handle internally
    if( destination == NULL ) {
        string src(source), dest("");

        int period = src.find('.');

        if( period > 0 ) {
            for( int i = 0; i < period; i++ )
                dest += src[i];
        }
        else
            dest = src;

        // Get file name, change extension to .html
        dest += ".html";

        // Define destination
        destination = new char[ dest.length() ];
        strcpy( destination, dest.c_str() );
    }

    // Return whatever kavascript gives us
    return ktml( source, destination, flags );
}
